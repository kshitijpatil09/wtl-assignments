package com.kshitijpatil.spring;

import org.springframework.stereotype.Component;

@Component
public class FortuneService implements Fortune {

	@Override
	public String getDailyFortune() {
		return "Daily sports fortune";
	}

}
