package com.kshitijpatil.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("cricket")
@Scope("prototype")
public class CricketCoach implements Coach {
	@Autowired
	FortuneService fortuneService;
	String name;
	String country;
	

	public CricketCoach(String name, String country) {
		this.name = name;
		this.country = country;
	}

	@Override
	public String toString() {
		return "CricketCoach [name=" + name + ", country=" + country + "]";
	}

	public CricketCoach() {
		// New instance for every call due to prototype
		System.out.println("Cricket coach created");
	}

	@Override
	public String getDailyWorkout() {
		return "Daily Cricket Workout";
	}
	
	public String getFortune() {
		return fortuneService.getDailyFortune();
	}
}
