package com.kshitijpatil.spring;

public interface Coach {
	String getDailyWorkout();
}
