<?php
    require('config/config.php');
    require('config/db.php');

    $msg = '';

    // Check submit
    if (isset($_POST['submit'])) {
        $rollno = mysqli_real_escape_string($conn, $_POST['rollno']);
        $name = mysqli_real_escape_string($conn, $_POST['name']);
        $dept = mysqli_real_escape_string($conn, $_POST['dept']);
        if (empty($rollno) || empty($name) || empty($dept)) {
            $msg = 'Please fill in all the fields';
        } else {
            $query = "INSERT INTO student VALUES('$rollno','$name','$dept')";

            if (mysqli_query($conn, $query)) {
                header('Location: '.ROOT_URL.'fetch.php');
            } else {
                echo 'ERROR: '.mysqli_error($conn);
            }
        }
    }

?>
<?php include('inc/header.php'); ?>
    <header id="main-header">
        <h2>PHP register new student</h2>
    </header>
    <section id="main">
        <div class="container">
            <?php if($msg !== ''): ?>
                <h5><?php echo $msg; ?></h5>
            <?php endif;?>
            <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="my-form">
                <div class="form-field">
                    <label for="rollno">Roll Number</label>
                    <input type="text" name="rollno">
                </div>
                <div class="form-field">
                    <label for="name">Name</label>
                    <input type="text" name="name">
                </div>
                <div class="form-field">
                    <label for="dept">Department</label>
                    <input type="text" name="dept">
                </div>
                <input type="submit" value="Submit" name="submit" class="button">
            </form>
        </div>
    </section>
<?php include('inc/footer.php'); ?>