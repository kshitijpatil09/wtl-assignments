<%@page import="javax.websocket.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Authentication</title>
<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<%!
		private final String USERNAME = "kshitij";
		private final String PASSWORD = "123456";
		public String message;
		
	%>
	<%
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		if (username.equals(USERNAME) && password.equals(PASSWORD)) {
			message = "Login Successful";
			session.setAttribute("loggedIn", true);
		} else {
			message = "Invalid credentials";
			session.setAttribute("loggedIn", false);
		}
	%>
	<form action="logout" class="my-form" method="post">
		<input type="submit" value="Logout" class="button">
	</form>
</body>
</html>