<?
    $html = `<table>
    <tr>
        <th>Company</th>
        <th>Contact</th>
        <th>Country</th>
    </tr>
    <tr>
        <td>Alfreds Futterkiste</td>
        <td>Maria Anders</td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>Centro comercial Moctezuma</td>
        <td>Francisco Chang</td>
        <td>Mexico</td>
    </tr>
    <tr>
        <td>Ernst Handel</td>
        <td>Roland Mendel</td>
        <td>Austria</td>
    </tr>
    <tr>
        <td>Island Trading</td>
        <td>Helen Bennett</td>
        <td>UK</td>
    </tr>
    <tr>
        <td>Laughing Bacchus Winecellars</td>
        <td>Yoshi Tannamuri</td>
        <td>Canada</td>
    </tr>
    <tr>
        <td>Magazzini Alimentari Riuniti</td>
        <td>Giovanni Rovelli</td>
        <td>Italy</td>
    </tr>
</table>`;
    /*** a new dom object ***/ 
    $dom = new domDocument; 
   
    /*** load the html into the object ***/ 
    $dom->loadHTML($html); 
    
    /*** discard white space ***/ 
    $dom->preserveWhiteSpace = false; 
    
    /*** the table by its tag name ***/ 
    $tables = $dom->getElementsByTagName('table'); 
    
    /*** get all rows from the table ***/ 
    $rows = $tables->item(0)->getElementsByTagName('tr'); 
    
    /*** loop over the table rows ***/ 
    foreach ($rows as $row) {
        /*** get each column by tag name ***/ 
        $cols = $row->getElementsByTagName('td'); 
        
        /*** echo the values ***/ 
        echo 'Company: '.$cols->item(0)->nodeValue.'<br />'; 
        echo 'Contact: '.$cols->item(1)->nodeValue.'<br />'; 
        echo 'Country: '.$cols->item(2)->nodeValue; 
        echo '<hr />'; 
    }
?>