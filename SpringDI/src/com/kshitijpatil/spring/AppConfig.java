package com.kshitijpatil.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.kshitijpatil.spring")
@PropertySource("classpath:coach.properties")
public class AppConfig {
	@Value("${name}")
	String name;
	
	@Value("${country}")
	String country;
	@Bean(name = {"customCoach"})
	Coach getCoach() {
		Coach cricketCoach = new CricketCoach(name, country);
		return cricketCoach;
	}
}