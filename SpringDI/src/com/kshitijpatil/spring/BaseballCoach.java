package com.kshitijpatil.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("baseball")
public class BaseballCoach implements Coach {
	@Autowired
	FortuneService fortuneService;
	
	@Override
	public String getDailyWorkout() {
		return "Daily Baseball workout";
	}
	
	public String getFortune() {
		return fortuneService.getDailyFortune();
	}
	
}
