/*
 * Template to intercept form submission for validation
 */
document.querySelector("#orderForm").addEventListener('submit', (e) => {
    e.preventDefault();
    console.log("Call intercepted...");
    document.querySelector("#id").value = 15;
    if (document.querySelector("#id").value !== 0) {
    	document.forms["orderForm"].submit();
    }
})