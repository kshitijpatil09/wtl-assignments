package com.kshitijpatil.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringStudentsApplication {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		Student student = context.getBean("student", Student.class);
		System.out.println(student);
		
		student = context.getBean("studentTwo", Student.class);
		System.out.println(student);
		
	}

}
