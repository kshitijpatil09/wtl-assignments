import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/calculateTax")
public class TaxServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter writer = res.getWriter();
		double salary = Double.parseDouble(req.getParameter("salary"));
		double taxPayable;
		if (salary < 250000) {
			taxPayable = 0.0;
		} else if (salary > 250000 && salary < 500000) {
			taxPayable = 0.10 * salary;
		} else {
			taxPayable = 0.20 * salary;
		}
		StringBuilder sb = new StringBuilder().append("Tax payable: ").append(taxPayable);
		System.out.println("Tax payable: " + sb.toString());
		
		writer.println(sb.toString());
	}
}
