angular.module('calcApp',[])
    .controller('CalcController',['$scope', ($scope) => {
        let char = '';
        let currentOp = '';
        const ops = ['+','-','*','/']
        $scope.result = 0;
        let lastNumPos = 0;
        let currentNum = 0;
        let lastNum = 0;
        $scope.handleExpression = () => {
            // console.log($scope.expression);
            if ($scope.expression === '') {
                $scope.result = 0;
                currentOp = '';
            }
            char = $scope.expression.substr(-1);
            if (ops.includes(char)) {
                currentOp = char;
                lastNumPos = $scope.expression.length;
                lastNum = -1;
            } else {
                // console.log(lastNum);
                console.log(`slicing ${lastNumPos},${$scope.expression.length}`);
                currentNum = parseInt($scope.expression.slice(lastNumPos,$scope.expression.length));
                switch(currentOp) {
                    case '+':
                        $scope.result += currentNum;
                        break;
                    case '-':
                        $scope.result -= currentNum;
                        break;
                    case '*':
                        $scope.result *= currentNum;
                        break;         
                    case '/':
                        $scope.result /= currentNum;
                        break;                                       
                    case '':
                        $scope.result = currentNum;
                }
                if (lastNum != -1) {
                    switch(currentOp) {
                        case '+':
                            $scope.result -= lastNum;
                            break;
                        case '-':
                            $scope.result += lastNum;
                            break;
                        case '*':
                            $scope.result /= lastNum;
                            break;         
                        case '/':
                            $scope.result *= lastNum;
                            break;                                       
                    }
                }
                lastNum = parseInt($scope.expression.slice(lastNumPos,$scope.expression.length));
            }
        }
    }]);