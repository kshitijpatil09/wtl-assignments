import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * @author kshitij
 *	First Servlet: to store product details into database (Mock)
 */
@WebServlet("/storeDetails")
public class StoreDetailsServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));
		String name = req.getParameter("name");
		float price = Float.parseFloat(req.getParameter("price"));
		int quantity = Integer.parseInt(req.getParameter("quantity"));
		
		
		HttpSession session = req.getSession();
		session.setAttribute("product_id", id);
		
		System.out.println("Storing product details..");
		System.out.println("ID: "+ id);
		System.out.println("Name: " + name);
		System.out.println("Price: " + price);
		System.out.println("Quantity: " + quantity);
		
		RequestDispatcher rd = req.getRequestDispatcher("payment");
		rd.forward(req, res);
	}
}
