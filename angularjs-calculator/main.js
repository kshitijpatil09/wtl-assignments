angular.module('calcApp',[])
    .controller('CalcController', ($scope) => {
        $scope.result = 0;
        $scope.op = '+';
        const performOperation = () => {
            switch($scope.op) {
                case '+':
                    $scope.result = (parseInt($scope.num1) || 0) + (parseInt($scope.num2) || 0);
                    break;
                case '-':
                    $scope.result = (parseInt($scope.num1) || 0) - (parseInt($scope.num2) || 0);
                    break;
                case '*':
                    $scope.result = (parseInt($scope.num1) || 0) * (parseInt($scope.num2) || 0);
                    break;
                case '/':
                    $scope.result = (parseInt($scope.num1) || 0) / (parseInt($scope.num2) || 0);
                    break;
            }
            
        }
        $scope.valueChanges = performOperation;
        $scope.setOp = (op) => {
            $scope.op = op;
            performOperation();
        }
    })