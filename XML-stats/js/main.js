/**
 * <input type="file" id="xmlFile" />
 * include this tag in your html file
 * you can use the following xml file for testing
 * URL to file: https://drive.google.com/file/d/1Ifef0RCqCq5apRT2LQbB6HguD43VpTXF/view?usp=sharing
 */

document.querySelector('#xmlFile').addEventListener('change', readFile, false);

function readFile(e) {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = parseXml;
    reader.readAsText(file);
}
const parseXml = (e) => {
    const xml = e.target.result;
    console.log(xml);
    let xmlDoc;
    if (window.DOMParser)
    {
        const parser = new DOMParser();
        xmlDoc = parser.parseFromString(xml, "text/xml");
    } else {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.loadXML(xml);
    }
    const records = xmlDoc.getElementsByTagName('employee');
    for (let i = 0; i < records.length; i++) {
        records[i].childNodes.forEach((node) => {
            if (node.nodeType == 1) {
                console.log(node.childNodes[0].nodeValue);
            }
        });
    }
};