<?php
    require('config/config.php');
    require('config/db.php');

    // Create Query
    $query = 'SELECT * FROM student';

    // Get results
    $result = mysqli_query($conn, $query);

    // Fetch data
    $students = mysqli_fetch_all($result, MYSQLI_ASSOC);
    // var_dump($students);

    // Free result
    mysqli_free_result($result);

    // Close connection
    mysqli_close($conn);
?>
<?php include('inc/header.php'); ?>
    <header id="main-header">
        <h2>PHP Mysql Student records</h2>
    </header>
    <section id="main">
        <div class="container">
            <table style="margin-top: 2rem">
                <thead>
                    <tr>
                        <td>Roll Number</td>
                        <td>Name</td>
                        <td>Department</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($students as $student) : ?>
                    <tr>
                        <td><?php echo $student['rollno']; ?></td>
                        <td><?php echo $student['name']; ?></td>
                        <td><?php echo $student['dept']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
<?php include('inc/footer.php'); ?>