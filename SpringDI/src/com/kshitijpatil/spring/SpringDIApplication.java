package com.kshitijpatil.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringDIApplication {

	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		
		Coach coach = context.getBean("baseball", BaseballCoach.class);
		System.out.println(coach.getDailyWorkout());
		System.out.println(((BaseballCoach)coach).getFortune());
		
		coach = context.getBean("cricket", CricketCoach.class);
		System.out.println(coach.getDailyWorkout());
		System.out.println(((CricketCoach)coach).getFortune());
		
		coach = context.getBean("cricket", CricketCoach.class);
		
		coach = context.getBean("customCoach", CricketCoach.class);
		System.out.println(coach);
	}

}
