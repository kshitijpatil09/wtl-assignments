import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * 
 * @author kshitij
 * Second Servlet: Payment procedure (Mock)
 */

@WebServlet("/payment")
public class PaymentServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		
		int id = (int) session.getAttribute("product_id");
		
		PrintWriter writer = res.getWriter();
		
		writer.println("Initiating payment procedure for Product ID: " + id);
	}
}
